import React, { Component } from 'react';
import logo from './moodys.jpg';
import './App.css';
import axios from 'axios';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [], Username: "",
      password: ""
    };
  };

  validateForm() {
    return this.state.Username.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
  }

  Auth() {
    axios.post(`http://localhost:8080/openam/json/realms/root/authenticate`)
      .then(res => {
        this.setState({ items: res.data });
        console.log(res);
        console.log(this.state.items);
      })
  }
  componentDidMount() {
    this.Auth();
  }

  render() {

    return (
      <div className="App">
        <nav className="navbar navbar-default navbar-fixed-top bg-custom">
          <div className="container">
            <div className="navbar-header ">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <a className="navbar-brand" href="#"><img src={logo} className="logo" /></a>
            </div>
            <div id="navbar" className="navbar-collapse collapse">
            </div>
          </div>
        </nav>

        <div className="row login">
          <div className="col-md-6 col-md-offset-3">
            <div className="Login">
              <form onSubmit={this.handleSubmit}>
                <FormGroup controlId="email" bsSize="large">
                  <ControlLabel className="pull-left">Username or Email</ControlLabel>
                  <FormControl className="form-control-sm"
                    autoFocus
                    type="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                </FormGroup>
                <FormGroup controlId="password" bsSize="large">
                  <ControlLabel className="pull-left">Password</ControlLabel>
                  <FormControl className="form-control-sm"
                    value={this.state.password}
                    onChange={this.handleChange}
                    type="password"
                  />
                </FormGroup>
                <Button
                  block
                  bsSize="large"
                  disabled={!this.validateForm()}
                  type="submit"
                >
                  Login
          </Button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
